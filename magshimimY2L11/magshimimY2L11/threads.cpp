#include "threads.h"


void I_Love_Threads()
{
	std::cout << "I love thread's!" << std::endl;
}

void call_I_Love_Threads()
{
	std::thread t1(I_Love_Threads);
	t1.join();
}

void printVector(std::vector<int> primes)
{
	for (int i = 0; i < primes.size(); i++)
		std::cout << primes[i] << " ";
	std::cout << std::endl;
}

void getPrimes(int begin, int end, std::vector<int>& primes)
{
	bool isPrime = true;

	for (int n = begin; n < end; n++)
	{
		for (int i = 2; i <= n / 2; ++i)
		{
			if (n % i == 0)
			{
				isPrime = false;
				break;
			}
		}
		if (isPrime)
		{
			primes.push_back(n);
		}
		isPrime = true;
	}
}

std::vector<int> callGetPrimes(int begin, int end)
{
	clock_t t;
	t = clock();
	std::vector<int> myPrimeVector = std::vector<int>();
	
	std::thread  a(getPrimes, begin, end, std::ref(myPrimeVector));
	a.join();
	t = clock() - t;
	double time_taken = ((double)t) / CLOCKS_PER_SEC;
	std::cout << "getPrimes took " << time_taken << " to execute" << std::endl;
	return myPrimeVector;
}

void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	std::string a = " ";
	bool isPrime = true;
	for (int n = begin; n < end; n++)
	{
		for (int i = 2; i <= n / 2; ++i)
		{
			if (n % i == 0)
			{
				isPrime = false;
				break;
			}
		}
		if (isPrime)
		{
			a += std::to_string(n);
			file << a;
		}
		isPrime = true;
	}
	file << std::endl;
}

void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	std::ofstream myFile;
	myFile.open(filePath);
	std::vector<std::thread*> myThreads;
	if (!myFile.is_open()) {
		std::cout << "Error opening the file!" << std::endl; 
		return;
	}
	int jump = (end - begin) / N, j = 0;
	for (int i = begin; i < end-jump; i +=jump, j++)
	{
		myThreads.push_back(new std::thread(writePrimesToFile, i, i+jump, std::ref(myFile)));
	}
	clock_t t;
	t = clock();
	for (int i = 0; i < N - 1; i++)
	{
		myThreads[i]->join();
	}
	t = clock() - t;
	double time_taken = ((double)t) / CLOCKS_PER_SEC;
	std::cout << "writePrimesToFile took " << time_taken << " to execute" << std::endl;
	myFile.close();
}

void Threads()
{
	int i = 0;
	std::thread next(PrintMyThread, i);
	next.join();
}

void PrintMyThread(int i)
{
	if (i == 51)
	{
		return;
	}
	std::cout << "Hello from thread " << i << std::endl;
	std::thread next(PrintMyThread, i+1);
	next.join();
}


void printTheColor()
{
	
	std::thread myArr[20];
	Color::Code color;
	for (int i = 0; i < 20; i++)
	{
		switch (i)
		{
			case 0:
			{
				color = Color::FG_RED;
				break;
			}
			case 1:
			{
				color = Color::FG_GREEN;
				break;
			}
			case 2:
			{
				color = Color::FG_BLUE;
				break;
			}
			case 3:
			{
				color = Color::FG_YELLOW;
				break;
			}
			case 4:
			{
				color = Color::FG_MAGENTA;
				break;
			}
			case 5:
			{
				color = Color::FG_CYAN;
				break;
			}
			case 6:
			{
				color = Color::FG_LIGHT_GRAY;
				break;
			}
			case 7:
			{
				color = Color::FG_DARK_GRAY;
				break;
			}
			case 8:
			{
				color = Color::FG_LIGHT_RED;
				break;
			}
			case 9:
			{
				color = Color::FG_LIGHT_GREEN;
				break;
			}
			case 10:
			{
				color = Color::FG_LIGHT_BLUE;
				break;
			}
			case 11:
			{
				color = Color::FG_LIGHT_YELLOW;
				break;
			}
			case 12:
			{
				color = Color::FG_LIGHT_MAGENTA;
				break;
			}
			case 13:
			{
				color = Color::FG_LIGHT_CYAN;
				break;
			}
			case 14:
			{
				color = Color::FG_LIGHT_GRAY;
				break;
			}
			case 15:
			{
				color = Color::FG_DARK_GRAY;
				break;
			}
			case 16:
			{
				color = Color::FG_WHITE;
				break;
			}
			case 17:
			{
				color = Color::BG_RED;
				break;
			}
			case 18:
			{
				color = Color::BG_GREEN;
				break;
			}
			case 19:
			{
				color = Color::BG_BLUE;
				break;
			}
			default:
			{
				color = Color::FG_DEFAULT;
				break;
			}
		}
	
		myArr[i] = std::thread(printColor, Color::Modifier(color));
	}

	for (int i = 0; i < 20; i++)
	{
		myArr[i].join();
	}
}

void printColor(Color::Modifier myColor)
{
	Color::Modifier defualtBackGround(Color::BG_DEFAULT);
	Color::Modifier defualt(Color::FG_DEFAULT);
	for (int i = 0; i < 15; i++)
	{
		std::cout << myColor << "color" << defualt << defualtBackGround;
	}
}

void max_with_threads()
{
	int RandomArray[1000];
	int max = 0;
	for (int i = 0; i < 1000; i++)
	{
		RandomArray[i] = rand() % MAX_NUM + 1;
	}
	std::thread myThreads[10];
	int maxs[10];
	int jump = 1000 / 10;
	for (int i = 0; i < 10; i++)
	{
		myThreads[i] = std::thread(find_max, i * jump, jump * i + jump, RandomArray, std::ref(maxs[i]));
	}
	for (int i = 0; i < 10; i++) { myThreads[i].join(); }
	for (int i = 0; i < 10; i++) {
		if (max < maxs[i]) max = maxs[i];
	}
	std::cout << max << " is the largest" << std::endl;
}

void find_max(int begin, int end, int* arr, int& maxs)
{
	int max = 0;
	for (int i = begin; i < end; i++)
	{
		if (max < arr[i])
		{
			max = arr[i];
		}
	}
	maxs = max;
}
