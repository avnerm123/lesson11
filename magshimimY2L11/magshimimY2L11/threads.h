#pragma once

#include <string>
#include <fstream>

#include <iostream>
#include<vector>
#include "color.h"
#include <thread>
#define MAX_NUM 10000

void I_Love_Threads();
void call_I_Love_Threads();

void printVector(std::vector<int> primes);

void getPrimes(int begin, int end, std::vector<int>& primes);
std::vector<int> callGetPrimes(int begin, int end);


void writePrimesToFile(int begin, int end, std::ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N);

void Threads();
void PrintMyThread(int i);

void printTheColor();
void printColor(Color::Modifier myColor);

void max_with_threads();
void find_max(int begin, int end, int* arr, int& maxs);

