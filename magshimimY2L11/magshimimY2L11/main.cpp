#include "threads.h"
#include "color.h"


int main()
{
	call_I_Love_Threads();
	system("pause");
	system("cls");
	max_with_threads();
	system("pause");
	system("cls");
	Threads();
	system("pause");
	system("cls");
	printTheColor();
	system("pause");
	system("cls");
	std::vector<int> primes1 = callGetPrimes(0, 1000);
	system("pause");
	system("cls");
	std::vector<int> primes2 = callGetPrimes(0, 100000);
	system("pause");
	system("cls");
	std::vector<int> primes3 = callGetPrimes(0, 1000000);
	system("pause");
	system("cls");
	callWritePrimesMultipleThreads(0, 1000, "primes2.txt", 1000);
	system("pause");
	system("cls");
	callWritePrimesMultipleThreads(0, 100000, "primes2.txt", 1000);
	system("pause");
	system("cls");
	callWritePrimesMultipleThreads(0, 1000000, "primes2.txt", 1000);
	system("pause");
	return 0;
}